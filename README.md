# hexo-buffifly-theme

#### Description
hexo蝴蝶主题魔改版

[hexo-buffifly-theme](https://gitee.com/glassyskyforgame/hexo-buffifly-theme)是基于Hexo主题butterfly魔改而成

### 如何使用

首先从码云下载到本地，放到空的文件夹，以免与其他文件混淆和覆盖
```
git clone https://gitee.com/glassyskyforgame/hexo-buffifly-theme.git
```
然后安装项目依赖

```
npm install 或者 yarn add
```

安装完之后就可以执行start脚本,生成静态目录，启动
Hexo-server

if you domain windows 
```
start.bat
```
if you domain linux or mac
```
start.sh
```
如果上面脚本执行没有问题，就可以打开浏览器输入***lolcalhost:4000***看到主页内容就成功了

&emsp;&emsp;此魔改主题预先装好Hexo-admin插件，可以访问***localhost:4000/buffifly/admin***就可以进入后台页面进行写作

由于主题基于[Buttifly](http://demo.jerryc.me/),基础配置请参考原作者的配置教程。

>   [传送门](http://demo.jerryc.me/)

Hexo官方文档

> [Hexo](https://hexo.io/zh-cn/docs/)

<br>
<br>
<br>
<br>

### 魔改记录 
>   [魔改记录](ChangeLog.md)

### 声明：此项目仅供博客搭建的学习和研究，如有商业问题，不承担任何后果 
